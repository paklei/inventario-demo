Este proyecto utiliza Gradle como APK, por lo que es un requisito para poder ejecutarlo.

Es necesario modificar las credenciales de la base de datos escritas en el archivo **src/main/resources/application.properties **.

El archivo **db.changelog-master.yaml** que se encuentra en la carpeta **resources** contiene el código necesario para que liquibase pueda generar la migración.

Actualmente el proyecto puede registrar productos y sus entradas y salidas ... aún no están escritos los tests.

**TO-DO**  
List < Movimiento >