package mx.corp.medalfa.inventario.service;

import java.util.Date;
import java.util.List;

import mx.corp.medalfa.inventario.entity.Movimiento;

public interface MovimientoService {
	public Object registrarEntrada(Integer productoId,Integer cantidad,Date fecha,String lote, Date fechaCaducidad);
	public Object registrarSalida(Integer productoId,Integer cantidad,Date fecha,String lote);
	public List<Movimiento> getMovimientosPorPeriodo(Date fechaInicial, Date fechaFinal);
	public List<Movimiento> getMovimientosPorProducto(Integer productoId);
	public Integer getExistenciasActuales(Integer productoId);
}
