package mx.corp.medalfa.inventario.service;

public interface ProductoService {
	public Object registrar(String nombre);
	public Object desactivar(Integer id);
}
