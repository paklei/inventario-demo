package mx.corp.medalfa.inventario.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.corp.medalfa.inventario.service.ProductoService;

@RestController
@RequestMapping("/producto/")
public class ProductoController {
	@Autowired
	ProductoService productoService;
	
	@PostMapping("registrar")
	public Object registrarEntrada(@RequestParam(value="nombre", required = true) String nombre) {
		return productoService.registrar(nombre);
	}
}
