package mx.corp.medalfa.inventario.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import mx.corp.medalfa.inventario.service.MovimientoService;

@RestController
public class MovimientoController {

	@Autowired
	MovimientoService movimientoService;
	
	@PostMapping("/entrada")
	public Object registrarEntrada(@RequestParam(value="cantidad", required = true) Integer cantidad,
			@RequestParam(value="fecha", required = true) Date fecha,
			@RequestParam(value="fecha_caducidad", required = true) Date fechaCaducidad,
			@RequestParam(value="producto_id", required = true) Integer productoId,
			@RequestParam(value="lote", required = true) String lote) {
		return movimientoService.registrarEntrada(productoId, cantidad, fecha, lote, fechaCaducidad);
	}
	
	@PostMapping("/salida")
	public Object registrarEntrada(@RequestParam(value="cantidad", required = true) Integer cantidad,
			@RequestParam(value="fecha", required = true) Date fecha,
			@RequestParam(value="producto_id", required = true) Integer productoId,
			@RequestParam(value="lote", required = true) String lote) {
		return movimientoService.registrarSalida(productoId, cantidad, fecha, lote);
	}
}
