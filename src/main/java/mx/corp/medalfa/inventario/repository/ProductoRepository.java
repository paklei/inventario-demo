package mx.corp.medalfa.inventario.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import mx.corp.medalfa.inventario.entity.Producto;

@Repository
public interface ProductoRepository extends CrudRepository <Producto, Integer>{

}
