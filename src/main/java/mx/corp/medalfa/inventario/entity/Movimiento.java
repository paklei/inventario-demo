package mx.corp.medalfa.inventario.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "movimientos")
public class Movimiento {
	   
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	
	@Column(updatable = false)
	private String lote;
	
	@Column(updatable = false)
	private Integer productoId;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public Integer getProductoId() {
		return productoId;
	}

	public void setProductoId(Integer productoId) {
		this.productoId = productoId;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public Date getCaducidad() {
		return caducidad;
	}

	public void setCaducidad(Date caducidad) {
		this.caducidad = caducidad;
	}

	public Integer getTipo() {
		return tipo;
	}

	public void setTipo(Integer tipo) {
		this.tipo = tipo;
	}

	public Integer getCantidad() {
		return cantidad;
	}

	public void setCantidad(Integer cantidad) {
		this.cantidad = cantidad;
	}

	@Column(updatable = false)
	private Date fecha;
	
	@Column(updatable = false)
	private Date caducidad;
	
	@Column(updatable = false)
	private Integer tipo;
	
	@Column(updatable = false)
	private Integer cantidad;
	
	public Movimiento() {
		
	}
	
	//entrada
	public Movimiento(Integer productoId, Integer cantidad, Date fecha,String lote, Date fechaCaducidad,Integer tipo) {
		this.productoId = productoId;
		this.cantidad = cantidad;
		this.fecha = fecha;
		this.lote = lote;
		this.caducidad = fechaCaducidad;
		this.tipo = tipo;
	}
	
	//salida
	public Movimiento(Integer productoId, Integer cantidad, Date fecha,String lote,Integer tipo) {
		this.productoId = productoId;
		this.cantidad = cantidad;
		this.fecha = fecha;
		this.lote = lote;
		this.caducidad = null;
		this.tipo = tipo;
	}
}
