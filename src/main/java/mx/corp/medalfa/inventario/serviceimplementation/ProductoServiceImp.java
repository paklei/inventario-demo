package mx.corp.medalfa.inventario.serviceimplementation;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.corp.medalfa.inventario.entity.Producto;
import mx.corp.medalfa.inventario.repository.ProductoRepository;
import mx.corp.medalfa.inventario.service.ProductoService;

@Service
public class ProductoServiceImp implements ProductoService{

	@Autowired
	ProductoRepository productoRepository;
	
	@Override
	public Object registrar(String nombre) {
		Producto producto = new Producto(nombre);
		productoRepository.save(producto);
		return new ResponseEntity("Producto registrado correctamente", HttpStatus.CREATED);
	}

	@Override
	public Object desactivar(Integer id) {
		Producto producto = productoRepository.findById(id).get();
		producto.setActivo(false);
		productoRepository.save(producto);
		return new ResponseEntity("Producto desactivado correctamente", HttpStatus.CREATED);
	}

}
