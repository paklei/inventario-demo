package mx.corp.medalfa.inventario.serviceimplementation;

import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import mx.corp.medalfa.inventario.entity.Movimiento;
import mx.corp.medalfa.inventario.entity.Producto;
import mx.corp.medalfa.inventario.entity.TipoMovimiento;
import mx.corp.medalfa.inventario.repository.MovimientoRepository;
import mx.corp.medalfa.inventario.repository.ProductoRepository;
import mx.corp.medalfa.inventario.service.MovimientoService;

@Service
public class MovimientoServiceImp implements MovimientoService {

	@Autowired
	MovimientoRepository movimientoRepository;
	
	@Autowired
	ProductoRepository productoRepository;
	
	@Override
	public Object registrarEntrada(Integer productoId, Integer cantidad, Date fecha,String lote, Date fechaCaducidad) {
		try {
			boolean isEntrada = true;
			actualizarExistencia(productoId,cantidad,isEntrada);
			
			Movimiento movimiento = new Movimiento(
					productoId,cantidad,fecha,lote,fechaCaducidad,TipoMovimiento.tipos.ENTRADA.ordinal());
			movimientoRepository.save(movimiento);
			return new ResponseEntity("Salida registrada correctamente", HttpStatus.CREATED);
		}catch (Exception e){
			e.printStackTrace();
		}
		return new ResponseEntity("Error al registrar Entrada", HttpStatus.CONFLICT);
	}

	@Override
	public Object registrarSalida(Integer productoId, Integer cantidad, Date fecha,String lote) {
		try {
			if(validarExistenciaSuficienteSalida(productoId, cantidad)) {
				boolean isEntrada = false;
				actualizarExistencia(productoId,cantidad,isEntrada);
				
				Movimiento movimiento = new Movimiento(
						productoId,cantidad,fecha,lote,TipoMovimiento.tipos.SALIDA.ordinal());
				movimientoRepository.save(movimiento);
				return new ResponseEntity("Salida registrada correctamente", HttpStatus.CREATED);
			}else {
				return new ResponseEntity("La existencia actual no es suficiente", HttpStatus.BAD_REQUEST);
			}
		}catch (Exception e){
			e.printStackTrace();
		}
		return false;
	}

	@Override
	public List<Movimiento> getMovimientosPorPeriodo(Date fechaInicial, Date fechaFinal) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Movimiento> getMovimientosPorProducto(Integer productoId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer getExistenciasActuales(Integer productoId) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private void actualizarExistencia(Integer productoId,Integer cantidad,boolean isEntrada) {
		Producto producto = productoRepository.findById(productoId).get();
		Integer existenciaFinal = isEntrada ? producto.getExistencia()+cantidad : producto.getExistencia()-cantidad;
		producto.setExistencia(existenciaFinal);
		productoRepository.save(producto);
	}
	
	private boolean validarExistenciaSuficienteSalida(Integer productoId, Integer cantidad) {
		Producto producto = productoRepository.findById(productoId).get();
		boolean existenciaSuficiente = producto.getExistencia()-cantidad >= 0 ? true : false;
		return existenciaSuficiente;
	}

}
